<?php
require_once("conexion.php");
Class Celsus
{
	public function ObtenerTodos($tabla)
	{	
        $conexion=new Conexion;
		$celsus=$conexion->consultar($tabla);
		return $celsus;
	}

   public function nuevo($datos, $tabla)
	{	$conexion=new Conexion;
        $celsus=$conexion->insertar($tabla,$datos);
        return $celsus;
	}

     public function Modificar($condicion, $datos, $tabla)
	{	$conexion=new Conexion;
        $celsus=$conexion->modificar($condicion,$datos,$tabla);
        return $celsus;
	}

    public function Filtrar($condicion, $tabla){
        $conexion=new Conexion;
		$celsus=$conexion->filtrar($condicion, $tabla);
		return $celsus;
    }

     public function Verificar($condicion, $tabla){
        $conexion=new Conexion;
		$celsus=$conexion->verificar($condicion, $tabla);
		return $celsus;
    }

    public function ObtenerFiltroFaccion($filtro, $tabla)
	{
		$conexion=new Conexion;
        $celsus=$conexion->consultarFiltro($tabla,$filtro);
		return $celsus;
	}

}
?>