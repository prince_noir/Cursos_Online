-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3307
-- Tiempo de generación: 20-12-2019 a las 13:51:45
-- Versión del servidor: 10.3.14-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbproyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tarjeta de Credito`
--
DROP TABLE IF EXISTS `Tarjeta`;
CREATE TABLE IF NOT EXISTS `Tarjeta` (
  `id_tarjeta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tarjeta` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tarjeta`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Llenado de tabla `Tarjeta`
--

INSERT INTO `Tarjeta` (`id_tarjeta`, `nombre_tarjeta`) VALUES
(1, 'Visa'),
(2, 'Dinners Club'),
(3, 'American Express');


--
-- Estructura de tabla para la tabla `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
CREATE TABLE IF NOT EXISTS `Usuario` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(20) NOT NULL,
  `correo_usuario` varchar(20) NOT NULL,
  `clave_usuario` varchar(20) NOT NULL,
  `pais` varchar(20) NULL,
  `numero_tarjeta` int(15) NULL,
  `fecha_expiracion` date  NULL,
  `cvc` varchar(4)  NULL,
  `codigo_postal` varchar(20)  NULL,
  `IdTarjeta` int(20)  NULL,
  CONSTRAINT `fk_IdTarjeta`
  FOREIGN KEY (IdTarjeta) REFERENCES Tarjeta(id_tarjeta),
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


--
-- Estructura de tabla para la tabla `Administrador`
--

DROP TABLE IF EXISTS `Administrador`;
CREATE TABLE IF NOT EXISTS `Administrador` (
  `id_administrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_administrador` varchar(20) NOT NULL,
  `correo_administrador` varchar(20) NOT NULL,
  `clave_administrador` varchar(20) NOT NULL,
  PRIMARY KEY (`id_administrador`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Llenado de tabla `Administrador`
--
INSERT INTO `Administrador` (`id_administrador`, `nombre_administrador`, `correo_administrador`, `clave_administrador`) VALUES
(1, 'admin', 'admin@hotmail.com', '1234');



--
-- Estructura de tabla para la tabla `Categoria`
--

DROP TABLE IF EXISTS `Categoria`;
CREATE TABLE IF NOT EXISTS `Categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(30) NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Llenado de tabla `Categoria`
--
INSERT INTO `Categoria` (`id_categoria`, `nombre_categoria`) VALUES
(1, 'Software'),
(2, 'Redes y Seguridad'),
(3, 'Hardware');


--
-- Estructura de tabla para la tabla `Creador`
--

DROP TABLE IF EXISTS `Creador`;
CREATE TABLE IF NOT EXISTS `Creador` (
  `id_creador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_creador` varchar(20) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` int(10) NOT NULL,
  PRIMARY KEY (`id_creador`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Llenado de tabla `Creador`
--
INSERT INTO `Creador` (`id_creador`, `nombre_creador`,`correo`,`telefono`) VALUES
(1, 'Diego Barroja','dbarrojs@hotmail.com','0928992142'),
(2, 'Ramon Maldonado','maldonadorn@hotmail.com','0991778074'),
(3, 'Alex Pagoada','lexpago@hotmail.com','0992436123'),
(4, 'Daniel Maldonado','malddan@hotmail.com','0956781232'),
(5, 'Joshue Samaniego','joshuasam@hotmail.com','0923247141'),
(6, 'Fabian Alarcon','alarconfab@hotmail.com','0925643562'),
(7, 'Pedro Fuentes','pedroronf@hotmail.com','0929522332'),
(8, 'Victoria Montoya','yavictoria@hotmail.com','0918154312');



--
-- Estructura de tabla para la tabla `Curso`
--

DROP TABLE IF EXISTS `Curso`;
CREATE TABLE IF NOT EXISTS `Curso` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_curso` varchar(50)  NOT NULL,
  `IdCreador` int(11) NOT NULL,
  `descripcion` varchar(150)  NOT NULL,
  `duracion` varchar(30)  NOT NULL,
  `dificultad` varchar(50)  NOT NULL,
  `url_portada` varchar(50)  NOT NULL,
  `recomendado` char(1) NOT NULL,
  `precio` double precision(3,2) NOT NULL,
  `IdCategoria` int(20) NOT NULL,
  CONSTRAINT `fk_IdCategoria`
  FOREIGN KEY (IdCategoria) REFERENCES Categoria(id_categoria),
  CONSTRAINT `fk_IdCreador`
  FOREIGN KEY (IdCreador) REFERENCES Creador(id_creador),
  PRIMARY KEY (`id_curso`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Llenado de tabla `Curso`
--

INSERT INTO `Curso` (`id_curso`, `nombre_curso`, `IdCreador`, `descripcion`, `duracion`, `dificultad`, `url_portada`, `recomendado`, `precio`, `IdCategoria`) VALUES
(1, 'Introduction to Programming',1,'Dive into a world of many possibilities','40 horas','Basico','programming_thumbnail.png','Y',9.99,1),
(2, 'Python',2,'Learn python to automate the boring stuff','50 horas','Intermedio','python_thumbnail.png','Y',14.99,1),
(3, 'C#',3,'Learn C# to create cool videogames','50 horas','Intermedio','c_sharp_thumbnail.png','N',14.99,1),
(4, 'CCNA',4,'Cybersecurity Operations','70 horas','Basico','cybersecurity_thumbnail.png','N',49.99,2),
(5, 'CCNA 1',5,'Indtroduction to Networks','50 horas','Intermedio','networks_thumbnail.png','N',59.99,2),
(6, 'CCNA 2',6,'Switching, Routing and Wireless','70 horas','Intermedio','switching_thumbnail.png','N',69.99,2),
(7, 'Arduino',7,'Learn electrical engineering basics to build circuits and program Arduino to make wearables, robots, and IoT devices','40 horas','Basico','arduino_thumbnail.png','Y',9.99,3),
(8, 'Raspberry Pi',8,'Learn about the Raspberry Pi, build a DIY Google Home Clone, RetroPie Gaming System and more.','40 horas','Basico','raspberry_thumbnail.png','N',19.99,3);


--
-- Estructura de tabla para la tabla `Factura`
--

DROP TABLE IF EXISTS `Factura`;
CREATE TABLE IF NOT EXISTS `Factura` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `subtotal` double(5,4) NOT NULL,
  `valor_iva` double(5,4) NOT NULL,
  `total` double(5,4) NOT NULL,
   PRIMARY KEY (`id_factura`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Estructura de tabla para la tabla `DetalleFactura`
--
DROP TABLE IF EXISTS `DetalleFactura`;
CREATE TABLE IF NOT EXISTS `DetalleFactura` (
  `id_detallefactura` int(11) NOT NULL AUTO_INCREMENT,
  `IdCurso` int(20) NOT NULL,
  `IdUsuario` int(20) NOT NULL,
  `IdFactura` int(20) NOT NULL,
  CONSTRAINT fk_IdUsuario
  FOREIGN KEY (IdUsuario) REFERENCES Usuario(id_user),
  CONSTRAINT fk_IdFactura
  FOREIGN KEY (IdFactura) REFERENCES Factura(id_factura),
  CONSTRAINT fk_IdCurso
  FOREIGN KEY (IdCurso) REFERENCES Curso(id_curso)
  ON DELETE CASCADE
  ON UPDATE RESTRICT,
  PRIMARY KEY (`id_detallefactura`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
