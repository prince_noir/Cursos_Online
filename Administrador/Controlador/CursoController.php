<?php
require_once("../Modelo/celsus.php");
$objCelsus=new Celsus;
switch($_POST['opcion'])
{
	case 'consultar':
		$datos=$objCelsus->ObtenerTodos('Curso');
		$tabla="";
		
		foreach($datos as $fila)
		{
			$tabla.="<tr>";
			$tabla.="<th scope='row'>".$fila['id_curso']."</th>";
			$tabla.="<td>".$fila['nombre_curso']."</td>";
            $tbcreador = $objCelsus->Filtrar("id_creador = '".$fila['IdCreador']."'", 'Creador');
            foreach($tbcreador as $creador){
                $tabla.="<td>".$creador['nombre_creador']."</td>";
            }			
			$tabla.="<td>".$fila['descripcion']."</td>";
			$tabla.="<td>".$fila['duracion']."</td>";
			$tabla.="<td>".$fila['dificultad']."</td>";
			$tabla.="<td>".$fila['url_portada']."</td>";
            $tabla.="<td>".$fila['recomendado']."</td>";
            $tabla.="<td>".$fila['precio']."</td>";
			$tbcategoria = $objCelsus->Filtrar("id_categoria = '".$fila['IdCategoria']."'", 'Categoria');
            foreach($tbcategoria as $categoria)
            {
                $tabla.="<td>".$categoria['nombre_categoria']."</td>";
            }
			$tabla.="<td><button type='button' class='btn btn-outline-dark' onclick='editar(".$fila['id_curso'].")'>Editar</button></td>";
			$tabla.="<tr>";
		}
		echo $tabla;
		break;
		
	case 'ingresar':
		$datos['nombre_curso']=$_POST['nombre_curso'];
		$datos['nombre_instructor']=$_POST['nombre_instructor'];
		$datos['descripcion']=$_POST['descripcion'];
		$datos['duracion']=$_POST['duracion'];
		$datos['dificultad']=$_POST['dificultad'];
		$datos['url_portada']=$_POST['url_portada'];
		$datos['IdCategoria']=$_POST['IdCategoria'];
			if($objCelsus->nuevo($datos, 'Curso'))
			{
				echo "Registro ingresado";
			}
			else
			{
				echo "Error al registrar".$objCelsus->geterror();
			}
		break;
		
	case 'actualizar':
		$filtro['id_curso']=$_POST['id_curso']; 
		$datos['nombre_curso']=$_POST['nombre_curso'];
		$datos['nombre_instructor']=$_POST['nombre_instructor'];
		$datos['descripcion']=$_POST['descripcion'];
		$datos['duracion']=$_POST['duracion'];
		$datos['dificultad']=$_POST['dificultad'];
		$datos['url_portada']=$_POST['url_portada'];
		$datos['IdCategoria']=$_POST['IdCategoria'];
		echo $datos=$objCelsus->Guardar($datos,'Curso',$filtro);
		break;
		
	case 'consultaxcodigo':
		$filtro['id_curso']=$_POST['id_curso'];
		echo json_encode($datos=$objCelsus->ObtenerFiltro('Curso',$filtro));
		break;
		
	
}
?>