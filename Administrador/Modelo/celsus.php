<?php
require_once("conexion.php");
Class Celsus
{
    public function Obtener($id, $campo, $tabla)
    {
        $conexion = new Conexion;
        $celsus = $conexion->consultarCelda($id, $campo, $tabla);
        return $celsus;
    }

	public function ObtenerTodos($tabla)
	{	$conexion=new Conexion;
		$celsus=$conexion->consultar($tabla);
		return $celsus;
	}

    public function Filtrar($condicion, $tabla){
        $conexion=new Conexion;
		$celsus=$conexion->filtrar($condicion, $tabla);
		return $celsus;
    }

	public function nuevo($datos, $tabla)
	{	$conexion=new Conexion;
		$celsus=$conexion->insertar($tabla,$datos);
		return $celsus;
	}
	public function Guardar($datos,$tabla,$filtro)
	{	$conexion=new Conexion;
		$celsus=$conexion->actualizar($tabla,$datos,$filtro);
		return $celsus;
	}
	
	public function ObtenerFiltro($tabla, $filtro)
	{
		$conexion=new Conexion;
		$celsus=$conexion->consultarFiltro($tabla,$filtro);
		return $celsus;
	}
	
	
	
}
?>