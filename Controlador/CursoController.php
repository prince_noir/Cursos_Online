<?php
require_once("../Modelo/celsus.php");
$objCelsus=new Celsus;
switch($_POST['opcion'])
{
	case 'consultar':
        
		$categoria_table=$objCelsus->ObtenerTodos('Categoria');
		$html="";

        foreach($categoria_table as $categoria){
             $html.="<h4 style='color: rgb(0, 204, 136);' id=".str_replace(' ','',$categoria['nombre_categoria']).">".$categoria['nombre_categoria']."</h4><br />";
            $html.="<div class='row'>";
            $curso_table=$objCelsus->Filtrar("IdCategoria = ".$categoria['id_categoria']."",'Curso');
            foreach($curso_table as $curso){
                $creador_table=$objCelsus->Filtrar("id_creador = ".$curso['IdCreador']."",'Creador');
                foreach($creador_table as $creador){
                     $html.="<div class='col-sm-12 col-md-6 col-xl-3 mb-4'>
                                <div class='card h-100 bg-dark_grey'>
                                    <div class='card-header'><b>".$curso['nombre_curso']."</b></div>
                                    <img src='../../Resources/".$curso['url_portada']."' class='card-img-top' alt='...'>
                                    <div class='card-body d-flex flex-column'>
                                        <p class='card-text'><b>Creador:</b> ".$creador['nombre_creador']."</p>
                                        <button class='mt-auto btn btn-outline-cyan stretched-link' onclick='verCurso(".$curso['id_curso'].")'>Start Course</button>
                                    </div>
                                </div>
                            </div>";
                }
            }
            $html.='</div><br /><br />';
        }
		
		echo $html;
		break;
	case 'consultar-featured':
        $curso_table=$objCelsus->Filtrar("recomendado = 'Y'",'Curso');
        $html="";
        foreach($curso_table as $curso){
            $creador_table=$objCelsus->Filtrar("id_creador = ".$curso['IdCreador']."",'Creador');
            foreach($creador_table as $creador){
                            $html.="<div class='col-sm-12 col-md-6 col-xl-3 mb-4'>
                                <div class='card h-100 bg-dark_grey'>
                                    <div class='card-header'><b>".$curso['nombre_curso']."</b></div>
                                    <img src='../../Resources/".$curso['url_portada']."' class='card-img-top' alt='...'>
                                    <div class='card-body d-flex flex-column'>
                                        <p class='card-text'><b>Creador:</b> ".$creador['nombre_creador']."</p>
                                        <button class='mt-auto btn btn-outline-cyan stretched-link' onclick='verCurso(".$curso['id_curso'].")'>Start Course</button>
                                    </div>
                                </div>
                            </div>";
            }
        }
        echo $html;
        break;
    case 'consultar-curso':
        $curso_table=$objCelsus->Filtrar("id_curso = ".$_POST['id_curso']."",'Curso');
        $html="";
        foreach($curso_table as $curso){
            $creador_table=$objCelsus->Filtrar("id_creador = ".$curso['IdCreador']."",'Creador');
            foreach($creador_table as $creador){
                $html.="<div class='col-sm-12 col-md-6 col-xl-3 mb-4'>
                            <div class='container'>
                                <h1 style='color: rgb(0, 204, 136);'><b>".$curso['nombre_curso']."</b></h1><br />
                                <h5>".$curso['descripcion']."<h5><br />
                                <h6><b>Creador</b>: ".$creador['nombre_creador']."</h6>
                                <h6><b>Duracion</b>: ".$curso['duracion']."</h6>
                                <h6><b>Dificultad</b>: ".$curso['dificultad']."</h6>
                            </div>
                        </div>
                        <div class='col-sm-12 col-md-6 col-xl-3 mb-4'>
                            <div class='card h-100 bg-dark_grey'>
                                <img src='../../Resources/".$curso['url_portada']."' class='card-img-top' alt='...'>
                                <div class='card-body'>
                                    <h2><b>$".$curso['precio']."</b><h2>
                                    <button id='cart' class='mt-auto btn btn-cyan mb-1' onclick='agregarDetalleFactura()'>Add to Cart</button>
                                    <button id='buy' class='mt-auto btn btn-outline-cyan'>Buy now</button>
                                </div>
                            </div>
                        </div>";
             }
        }
        echo $html;
        break;
}
?>